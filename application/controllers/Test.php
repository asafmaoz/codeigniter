<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // Your own constructor code
    }


    public function index(){
        echo 'Hello World';
    }

    /**
     * Url: http://codeigniter.dev/Test/checkparams/paramone/paramtwo
     * @param $paramOne
     * @param $paramTwo
     */
    public function checkParams($paramOne,$paramTwo){
        echo '<pre>';
        echo "This is param one: $paramOne <br>param two: $paramTwo";
    }


    /**
     * Remap method calls
     * @param $methodName
     * @param array $params
     * @return mixed
     */
    public function _remap($methodName, $params = array()){
        if (method_exists($this, $methodName))
        {
            return call_user_func_array(array($this, $methodName), $params);
        }
        show_404();
    }

    /**
     * Loading views, sending data to views
     */
    public function viewTutorial(){
        //load two views
        $welcomData = $this->load->view('welcome_message','',true);
        $data = array('name'=>'my name','age'=>32,'welcome_data'=>$welcomData);
        $this->load->view('testview',$data);
    }

    /**
     * Load a model
     */
    public function modelTutorial(){
        $this->load->model('test_model','',true);
        $this->test_model->insert_entry(array('title'=>'this test title','content'=>'this test content'));
    }

    public function checkHelper(){
        $this->load->helper('url');
        $data = array('name'=>'my name','age'=>32);
        $this->load->view('testview',$data);
    }

    public function checkLibrary(){
        $this->load->library('someclass',array('type' => 'large', 'color' => 'red'));
        echo $this->someclass->some_method();
        print_r($this->someclass->params);
        echo '<pre>';print_r($this->someclass);
    }


}














