<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Someclass {

    public $params;
    protected $CIobj;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = serialize($params);
        $this->CIobj = & get_instance();
    }

    public function some_method()
    {
        return 5;
    }
}